FROM ubuntu:xenial
MAINTAINER Fabian Laule <mail@fabianlaule.com>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q && apt-get install -qy \
    texlive-full \
    biber \
    wget \
    make git \
    && rm -rf /var/lib/apt/lists/*

RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime \
    && dpkg-reconfigure -f noninteractive tzdata

WORKDIR /data
VOLUME ["/data"]