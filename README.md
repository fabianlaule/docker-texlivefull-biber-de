# Docker Image "Docker-TexLiveFull-Biber-DE"

This repository holds the Dockerfile for building a new image with a full 
installation of `TexLive` and Biber. Additionally, the locale of the system is 
set to `Europe\Berlin`.

# Why this Image/Repository

This repository / image was created for the usage of the continous 
integration-features of GitLab.

# Example for GitLab-CI

```bash
image: registry.gitlab.com/fabianlaule/docker-texlivefull-biber-de:latest

build:
  script:
    - latexmk -pdf dokumentation.tex
    - mv dokumentation.pdf "My current thesis.pdf"
  artifacts:
    when: on_success
    expire_in: 2 mos
    paths:
      - "*.pdf"
```